## Liya Manager
A package installer with a easy syntax.

# Syntax
To Install - lmr-i <name of the package>
<br>
To Remove - lmr-r <name of the package>
<br>
To Install AUR Package - lmr-aur <name of package>
# New Packages
New package requests can be filed here
<br>
https://gitlab.com/liya5/pkgdb
<br>
Thanking You...
